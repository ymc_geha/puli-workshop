The Puli Workshop Repository
============================

Getting Started
---------------

Go to the `workshops/puli/` directory on the **host machine** and run:

```
$ git pull
```

Tip: If this command fails due to modified files in your directory, run the following and try again:

```
$ git checkout -- .
$ git clean -f
```

Once you pulled the latest changes, run:

```
$ bin/install-host
```

Tip: If Git or Composer is not installed on your host machine, you need to run `bin/install-host` in the vagrant box. However, performance will be much slower due to NFS.

Run your vagrant box (if not already running) and connect with SSH:

```
$ vagrant up --no-provision
$ vagrant ssh
```

Now run the following on the **guest machine**:

```
$ cd /var/www/summercamp/workshops/puli
$ bin/install-guest
```

Finally - still on the guest machine - run the server:

```
$ bin/run-server
```

You should be able to open http://puli.phpsc:8040/app_dev.php in your browser. The expected result is a `Twig_Loader_Error`.

Troubleshooting
---------------

If you can't connect with your browser, make sure the following line is included in your hosts file on the host machine:

```
172.21.12.10 puli.phpsc
```

